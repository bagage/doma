FROM library/php:5-fpm

RUN docker-php-ext-install mysqli

RUN apt-get update && apt-get install -y \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
	&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-install gd

# required for "file_get_contents" PHP method
RUN apt-get install -y libcurl3-dev \
	&& docker-php-ext-install curl

# mail sending
RUN apt-get install -y msmtp

